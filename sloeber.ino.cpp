#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-03-02 18:52:09

#include "Arduino.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include <ESP8266HTTPClient.h>
#include <Log.h>
#include <Test.h>
#include <Util.h>
#include <Config.h>
#include <WebSocketHandler.h>
#include <IotServer.h>
#include <ArduinoJson.h>
#include "AppConfig.h"

void wsLog(const char* severity, const char* tag, const char* msg);
void setup() ;
void loop() ;
void onConfig(int client, String incoming) ;
void onPing(int client, String incoming) ;
void updateConfigBuffer() ;
const char* wifi_ssid() ;
const char* wifi_pw() ;
const char* iot_server() ;
const char* iot_token() ;
IPAddress wifiIp();
IPAddress wifiGateway();
IPAddress wifiSubnet();

#include "ESP_App_Template.ino"

#include "local_conf.ino"

#endif
