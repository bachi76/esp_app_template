#ifndef APP_CONFIG
#define APP_CONFIG

#include <Log.h>
#include <ArduinoJson.h>

class AppConfig : public Config {

public:

	/* Define config settings */

	// The app password
	String password = "";

	// Number of successful and connections since start (not saved)
	int connectsSuccessful = 0;
	int connectsFailed = 0;

	// Example: Pings received, pongs sent (not saved)
	int pongs = 0;


	/* Load config values */
	void onLoad(JsonObject &json) {

		if (json.containsKey("password")) password = json["password"].asString();

		//if (json.containsKey("keyInt")) pumpFor = json["keyInt"];
		//if (json.containsKey("keyString")) keyString = String(json["keyString"].asString());
		//if (json.containsKey("keyChar")) keyChar =  strcpy(keyChar, json["keyChar"]);

	}

	/* Save r/w values */
	void onSave(JsonObject &json) {

		json["password"] = password;
	}
};

#endif APP_CONFIG
