/*
 * ESP_APP_Template
 *
 * Use this template to fast bootstrap your ESP + websocket-based Webapp project.
 * For the webapp template counterpart, see https://bitbucket.org/bachi76/iot-app-template
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include <ESP8266HTTPClient.h>
#include <Log.h>
#include <Test.h>
#include <Util.h>
#include <Config.h>
#include <WebSocketHandler.h>
#include <IotServer.h>
#include <ArduinoJson.h>

#include "AppConfig.h"


const String APP = "My ESP Demo";
const String VERSION = "0.9.0";
const char* TAG = "Main";

AppConfig config;
String cfgBuffer;

// IoTServer for remote data storage
IotServer iot(iot_server(), iot_token()); // TODO: demo token?

// Example
unsigned long lastMeasurement = 0;


// The websocket handler code
class MyWebSocketHandler : public WebSocketHandler {

public:

	const String TAG = "MyWS";
	const String appTag = APP + " " + VERSION;

	// Require a login only if a password is set
	bool onConnect(uint8_t num, uint8_t * payload, size_t length) {
		return config.password != "";
	};

	bool onVerifyLogin(uint8_t num, String incoming, size_t length) {
		return incoming == config.password;
	}

	void onLoginSuccessful(uint8_t num) {

		send(num, "msg", "Login successful");
		broadcast("msg", webSocket.remoteIP(num).toString() + " connected to " + appTag);
		//iot.post("login", webSocket.remoteIP(num).toString().c_str());

		//Send config to new user, update the connection counters
		webSocket.sendTXT(num, cfgBuffer);
	};

	void onDisconnect(uint8_t num, uint8_t * payload, size_t length) {

		broadcast("msg", "User" + String(num) + " disconnected from " + appTag);
		iot.post("logout", webSocket.remoteIP(num).toString().c_str());
	};
};

MyWebSocketHandler wsHandler;

// Log handler that sends entries to the connected websocket clients
void wsLog(const char* severity, const char* tag, const char* msg){
	wsHandler.log(severity, tag, msg);
}

// Read VCC on ADC
// ADC_MODE(ADC_VCC);

void setup()
{
	Log::addListener(wsLog);

	// Define command handlers
	wsHandler.addCommand("config", onConfig);
	wsHandler.addCommand("ping", onPing);

	// Transmit the rssi signal strength as hearbeat signal
	wsHandler.setHeartbeat(2000);

	Log::d(TAG, "Booting... ");
	cfgBuffer.reserve(512);
	config = AppConfig();
	config.load();
	updateConfigBuffer();

	Serial.begin(115200);
	Serial.setDebugOutput(true);

	// To use a static IP - remove if using DHCP
	//WiFi.config(wifiIp(), wifiGateway(), wifiSubnet());

	Util::connectToWifi(wifi_ssid(), wifi_pw(), WIFI_STA);

	//Log::enableRemoteLogging(iot);

	wsHandler.begin();

//	Serial.print("App password: ");
//	Serial.println(config.password);

	Log::i(TAG, "Initialized.");
	//beep(1);

}


void loop()
{

	// Checks wifi and try to reconnect if failed
	Util::keepConnected(wifi_ssid(), wifi_pw(), WIFI_STA);

	// Example: broadcast the adc value every 2s to all clients
	if (lastMeasurement + 10*1000 <= millis()) {
		wsHandler.broadcast("adc", analogRead(A0));
		lastMeasurement = millis();
	}

	// Must be called every few ms
	wsHandler.loop();
}


void onConfig(int client, String incoming) {
	config.loadFromJson(incoming);
	if (config.save()) {
		Log::i(TAG, "Config updated.");
		updateConfigBuffer();
		delay(100);
		//beep(3);

	} else {
		Log::e(TAG, "Config update failed.");
	}
}


void onPing(int client, String incoming) {
	wsHandler.broadcast("pong", ++config.pongs);
	wsHandler.send(client, "msg", "Got your ping, you got my pong!");
	Log::i(TAG, "Received a ping from user " + String(client));
}


/**
 * Update the json config string buffer that is sent to new clients
 */
void updateConfigBuffer() {

	cfgBuffer = "{\"config\":";
	config.getJson().printTo(cfgBuffer);
	cfgBuffer.concat("}");
};

/**
 * Beep signals (blocking, about 1/2s per beep) - if you have a beeper..
 */
//void beep(int beeps) {
//	for (int i=0; i<beeps; i++) {
//		digitalWrite(PIN_BEEPER, true);
//		delay(100);
//		webSocket.loop();
//		digitalWrite(PIN_BEEPER, false);
//		if (i+1 < beeps) delay(200);
//	}
//}

