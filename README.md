# ESP App Template #
This project helps to quickly bootstrap an ESP8266 / Arduino Core based project that connectes to a remote web app

### Features ###

* Set up an ESP Arduino project + [Web app project](https://bitbucket.org/bachi76/iot-app-template) in minutes
* Superfast client/server communication over websockes
* Client authentication
* Configuration handling including config editing in the web app
* Includes the [ESP Framework](https://bitbucket.org/bachi76/esp8266-framework) goodies
* Can send logs and data to a remote [IoT server](https://bitbucket.org/bachi76/iot-server) (featureing ElasticSearch / Kibana)


### Usage ###
1. Install the [ESP Framework](https://bitbucket.org/bachi76/esp8266-framework)
2. Fork this repo
3. Start coding


### Contributions ###
Are most welcome, please issue PRs.


### Legal note ###
Copyright 2017 Martin Bachmann, insign gmbh, www.insign.ch

Licensed under the *Apache License, Version 2.0* (the "License");
you may not use this library except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
